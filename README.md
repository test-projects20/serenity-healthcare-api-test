# Testing the API Healthcare using Serenity and Cucumber 6

This assignment is testing API end-points from the Healthcare.gov.

## Get the code

To get the code from GitLab, run the following:

    git clone https://gitlab.com/test-projects20/serenity-healthcare-api-test.git


## The project directory structure
The project can be built with Maven and has the following directory structure:
```Gherkin
src
  + main
  + test
    + java                                Test runners and supporting code
    + resources
      + features                          Feature files 
          + api
```

## Test scenarios
The project comes with two scenarios, one that tests the `topics` end-point, and a second that tests the `blog` end-point.

The first scenario is a positive scenario and tests that `https://www.healthcare.gov/api/topics.json` end-point returns response with status 200 
containing `/es/customer-service-topic/` in the body. 

The second scenario is a negative one, which tests that `https://www.healthcare.gov/api/blog` end-point that misses the format at the end (`.json`) returns a response with status 404 
containing the error message: `Sorry, we can't find that page` in the body. 


```Gherkin
Scenario: Healthcare topics end-point positive scenario
  Given the status of the API is 200
  When I make a request to the topics end-point
  Then the API response should contain "/es/customer-service-topic/"

Scenario: Healthcare blog end-point negative scenario
  Given the status of the API is 404 for incorrect end-point url
  When I send an incorrect request to the blog end-point
  Then the API response should contain "Sorry, we can't find that page"
```

The code below contains the definitions of the above Steps. 


```java
    @Given("the status of the API is 200")
    public void status_of_API_is_200() {
        assertThat(theApplication.topicsStatus()).isEqualTo(RUNNING);
    }

    @When("I make a request to the topics end-point")
    public void i_make_request_to_topics_endpoint() {
        theApplication.readTopicsMessage();
    }

    @Then("the API response should contain {string}")
    public void the_API_response_should_contain(String expectedMessage) {
        restAssuredThat(lastResponse -> lastResponse.body(containsString(expectedMessage)));
    }


        
    @Given("the status of the API is 404 for incorrect end-point url")
    public void status_of_API_is_404() {
        assertThat(theApplication.blogStatus()).isEqualTo(DOWN);
    }

    @When("I send an incorrect request to the blog end-point")
    public void i_send_an_incorrect_request_to_blog_endpoint() {
        theApplication.readBlogMessage();
    }
```

The actual REST calls to the `HealthcareAPI` are performed using `RestAssured` and `SerenityRest`.


```java
public class HealthcareAPI {

    public AppStatus topicsStatus() {
        int statusCode = RestAssured.get(TOPICS.getUrl()).statusCode();
        return (statusCode == 200) ? AppStatus.RUNNING : AppStatus.DOWN;
    }

    public AppStatus blogStatus() {
        int statusCode = RestAssured.get(BLOG_WRONG.getUrl()).statusCode();
        return (statusCode == 200) ? AppStatus.RUNNING : AppStatus.DOWN;
    }

    @Step("Get topics end-point message")
    public void readTopicsMessage() {
        SerenityRest.get(TOPICS.getUrl());
    }

    @Step("Get blog end-point message")
    public void readBlogMessage() {
        SerenityRest.get(BLOG_WRONG.getUrl());
    }
}
```


## Test report

You can generate full Serenity reports by running `mvn clean verify`.


![](src/docs/TestResults_success.PNG)

Below are the tests included in the scenarios: 

![](src/docs/Scenarios.PNG)

## Run tests in GitLab CI pipeline

To run the tests in the GitLab CI pipeline, I defined a job in `.gitlab-ci.yml` that runs the command `mvn clean verify`:

```yaml
image: maven:3.3.9-jdk-8

serenity-test-job:
  stage: test
  script:
    - mvn clean verify
```