package net.healthcare;

public enum WebServiceEndPoints {
    TOPICS("https://www.healthcare.gov/api/topics.json"),
    BLOG_WRONG("https://www.healthcare.gov/api/blog");

    private final String url;

    WebServiceEndPoints(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }
}
