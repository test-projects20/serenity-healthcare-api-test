package net.healthcare.api;

public enum AppStatus {
    RUNNING, DOWN
}
