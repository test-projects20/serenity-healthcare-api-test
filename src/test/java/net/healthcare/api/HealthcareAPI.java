package net.healthcare.api;

import io.restassured.RestAssured;
import net.healthcare.WebServiceEndPoints;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

public class HealthcareAPI {

    public AppStatus topicsStatus() {
        int statusCode = RestAssured.get(WebServiceEndPoints.TOPICS.getUrl()).statusCode();
        return (statusCode == 200) ? AppStatus.RUNNING : AppStatus.DOWN;
    }
    public AppStatus blogStatus() {
        int statusCode = RestAssured.get(WebServiceEndPoints.BLOG_WRONG.getUrl()).statusCode();
        return (statusCode == 200) ? AppStatus.RUNNING : AppStatus.DOWN;
    }

    @Step("Get topics end-point message")
    public void readTopicsMessage() {
        SerenityRest.get(WebServiceEndPoints.TOPICS.getUrl());
    }

    @Step("Get blog end-point message")
    public void readBlogMessage() {
        SerenityRest.get(WebServiceEndPoints.BLOG_WRONG.getUrl());
    }
}
