package net.healthcare.stepdefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import io.cucumber.java.en.Then;
import net.healthcare.api.AppStatus;
import net.healthcare.api.HealthcareAPI;
import net.thucydides.core.annotations.Steps;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.containsString;

public class ApplicationStatusStepDefinitions {

    @Steps
    HealthcareAPI theApplication;

    @Given("the status of the API is 200")
    public void status_of_API_is_200() {
        assertThat(theApplication.topicsStatus()).isEqualTo(AppStatus.RUNNING);
    }

    @When("I make a request to the topics end-point")
    public void i_make_request_to_topics_endpoint() {
        theApplication.readTopicsMessage();
    }

    @Then("the API response should contain {string}")
    public void the_API_response_should_contain(String expectedMessage) {
        restAssuredThat(lastResponse -> lastResponse.body(containsString(expectedMessage)));
    }


    @Given("the status of the API is 404 for incorrect end-point url")
    public void status_of_API_is_404() {
        assertThat(theApplication.blogStatus()).isEqualTo(AppStatus.DOWN);
    }

    @When("I send an incorrect request to the blog end-point")
    public void i_send_an_incorrect_request_to_blog_endpoint() {
        theApplication.readBlogMessage();
    }
}