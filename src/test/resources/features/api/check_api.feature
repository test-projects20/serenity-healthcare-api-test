Feature: Check Healthcare.gov API

  The `/api/topics.json` end point returns status 200 and the response contains "/es/customer-service-topic/"

  Scenario: Healthcare topics end-point positive scenario
    Given the status of the API is 200
    When I make a request to the topics end-point
    Then the API response should contain "/es/customer-service-topic/"

  Scenario: Healthcare blog end-point negative scenario
    Given the status of the API is 404 for incorrect end-point url
    When I send an incorrect request to the blog end-point
    Then the API response should contain "Sorry, we can't find that page"

